import streamlit as st
from streamlit_sweethome3d import streamlit_sweethome3d

import json

st.header("Sweet Home 3D")
st.markdown("This is an example of [streamlit_sweethome3d](https://gitlab.com/nicolalandro/streamlit-sweethome3d) plugin.")

in_state = {
    "homes": [
      {
        "walls": [
          [0, 180, 0, 400, 20, 250], # params: x1, y1 x2, y2, spessore, altezza
          [350, 180, 350, 400, 20, 250],
          [0-10, 180, 350+10, 180, 20, 250]
        ],
        "rooms": [
          [[0,180], [350, 180], [350, 400], [0, 400]] # points
        ],
        "furnitures": [
          {
            "id": "eTeks#shower",
            "x": 50,
            "y": 230,
            "elevation": 0,
          }
        ]
      }
    ]
}

out_state = streamlit_sweethome3d(state = in_state, out_state=True)

col1, col2 = st.columns(2)
with col1:
    st.text('in state')
    st.code(json.dumps(in_state, indent=1))
with col2:
    st.text('out state')
    st.code(json.dumps(out_state, indent=1))