import { Streamlit } from "streamlit-component-lib"
import _ from 'lodash'

var first_run = true;
var old_value = {};
var application;

function getNewVal() {
    return {
        "homes": [{
            "walls": application.getHomes()[0].walls.flat(1).map(w => [
                w.xStart,
                w.yStart,
                w.xEnd,
                w.yEnd,
                w.thickness,
                w.height
            ]),
            "rooms": application.getHomes()[0].rooms.map(r => r.getPoints()),
            "furnitures": application.getHomes()[0].furniture.map(f => {
                return {
                    "id": f.catalogId,
                    "x": f.x,
                    "y": f.y,
                    "elevation": f.getElevation(),
                }
            })
        }]
    }
}

function onRender(event) {
    const data = event.detail

    var urlBase = window.location.href.substring(0, window.location.href.lastIndexOf("/") + 1);
    if (!application) {
        application = new SweetHome3DJSApplication({
            // readHomeURL: urlBase + "data/%s.sh3x",
            // writeHomeURL: urlBase + "writeData.php?path=%s.sh3x",
            // writeResourceURL: urlBase + "writeData.php?path=%s",
            // readResourceURL: urlBase + "data/%s",
            // listHomesURL: urlBase + "listHomes.php",
            // deleteHomeURL: urlBase + "deleteHome.php?home=%s",
            // writePreferencesURL: urlBase + "writeData.php?path=userPreferences.json",
            // readPreferencesURL: urlBase + "data/userPreferences.json",
            furnitureCatalogURLs: [urlBase + "lib/resources/DefaultFurnitureCatalog.json"],
            furnitureResourcesURLBase: urlBase,
            texturesCatalogURLs: [urlBase + "lib/resources/DefaultTexturesCatalog.json"],
            texturesResourcesURLBase: urlBase,
            writeHomeWithWorker: false,
            compressionLevel: 5,
            autoRecovery: false,
            defaultHomeName: "home-d78b0c75-7b64-4a70-a722-37c8dd6a2b81"
        });

        application.addHome(application.createHome());
    }

    if (first_run) {
        first_run = false;
        data.args["state"]["homes"][0]['walls'].forEach(w => {
            application.getHomes()[0].addWall(new Wall(w[0], w[1], w[2], w[3], w[4], w[5]))
        })

        data.args["state"]["homes"][0]['rooms'].forEach(r => {
            var room = new Room(r);
            room.setCeilingVisible(false);
            application.getHomes()[0].addRoom(room);
        })

        data.args["state"]["homes"][0]['furnitures'].forEach(f => {
            var piece = new HomePieceOfFurniture(application.preferences.furnitureCatalog.getPieceOfFurnitureWithId(f['id']));

            piece.setX(f['x']);
            piece.setY(f['y']);
            piece.setElevation(f['elevation']);

            application.getHomes()[0].addPieceOfFurniture(piece);
        })
    }

    // generate DOC: that code print the doc that i put as md
    // var str = ""
    // str += '|id|category|object|icon|\n'
    // str += '|------|------|------|------|\n'
    // application.preferences.furnitureCatalog.getCategories().forEach(c => {
    //     c.furniture.forEach(f => {
    //         str += `|${f.id}|${c.name}|${f.name}|![](streamlit_sweethome3d/frontend/public${f.icon.url.slice(21)})|\n`
    //     })
    // });
    // console.log(str)

    // Get update
    if (data.args.out_state) {
        setInterval(
            function() {
                var new_value = getNewVal();
                if (!_.isEqual(new_value, old_value)) {
                    console.log('change')
                    old_value = new_value;
                    Streamlit.setComponentValue(old_value);
                }
            },
            1000
        );
    }


    Streamlit.setFrameHeight()
}

Streamlit.events.addEventListener(Streamlit.RENDER_EVENT, onRender)
Streamlit.setComponentReady()
Streamlit.setFrameHeight()