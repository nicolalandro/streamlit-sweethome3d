# Furniture list with id
|id|category|object|icon|
|------|------|------|------|
|eTeks#shower|Bagno|Doccia|![](streamlit_sweethome3d/frontend/public/lib/resources/models/shower.png)|
|eTeks#toiletUnit|Bagno|Gabinetto|![](streamlit_sweethome3d/frontend/public/lib/resources/models/toiletUnit.png)|
|eTeks#washbasin|Bagno|Lavandino|![](streamlit_sweethome3d/frontend/public/lib/resources/models/washbasin.png)|
|eTeks#washbasinWithCabinet|Bagno|Lavandino con mobiletto|![](streamlit_sweethome3d/frontend/public/lib/resources/models/washbasinWithCabinet.png)|
|eTeks#bath|Bagno|Vasca da bagno|![](streamlit_sweethome3d/frontend/public/lib/resources/models/bath.png)|
|eTeks#fittedBath|Bagno|Vasca da bagno incassata|![](streamlit_sweethome3d/frontend/public/lib/resources/models/fittedBath.png)|
|eTeks#hood|Cucina|Cappa|![](streamlit_sweethome3d/frontend/public/lib/resources/models/hood.png)|
|eTeks#cooker|Cucina|Cucina|![](streamlit_sweethome3d/frontend/public/lib/resources/models/cooker.png)|
|eTeks#kitchenCabinet|Cucina|Elemento cucina|![](streamlit_sweethome3d/frontend/public/lib/resources/models/kitchenCabinet.png)|
|eTeks#oven|Cucina|Forno|![](streamlit_sweethome3d/frontend/public/lib/resources/models/oven.png)|
|eTeks#fridge|Cucina|Frigorifero|![](streamlit_sweethome3d/frontend/public/lib/resources/models/fridge.png)|
|eTeks#fridgeFreezer|Cucina|Frigorifero & Freezer|![](streamlit_sweethome3d/frontend/public/lib/resources/models/fridgeFreezer.png)|
|eTeks#dishwasher|Cucina|Lavastoviglie|![](streamlit_sweethome3d/frontend/public/lib/resources/models/dishwasher.png)|
|eTeks#clothesWasher|Cucina|Lavatrice|![](streamlit_sweethome3d/frontend/public/lib/resources/models/clothesWasher.png)|
|eTeks#sink|Cucina|Lavello|![](streamlit_sweethome3d/frontend/public/lib/resources/models/sink.png)|
|eTeks#kitchenUpperCabinet|Cucina|Pensile|![](streamlit_sweethome3d/frontend/public/lib/resources/models/kitchenUpperCabinet.png)|
|eTeks#lamp|Luci|Lampada|![](streamlit_sweethome3d/frontend/public/lib/resources/models/lamp.png)|
|eTeks#pendantLamp|Luci|Lampada a sospensione|![](streamlit_sweethome3d/frontend/public/lib/resources/models/pendantLamp.png)|
|eTeks#workLamp|Luci|Lampada da lavoro|![](streamlit_sweethome3d/frontend/public/lib/resources/models/workLamp.png)|
|eTeks#wallUplight|Luci|Lampada da parete|![](streamlit_sweethome3d/frontend/public/lib/resources/models/wallUplight.png)|
|eTeks#floorUplight|Luci|Lampada da terra|![](streamlit_sweethome3d/frontend/public/lib/resources/models/floorUplight.png)|
|eTeks#spotlight|Luci|Riflettore|![](streamlit_sweethome3d/frontend/public/lib/resources/models/spotlight.png)|
|eTeks#halogenLightSource|Luci|Sorgente luce alogena|![](streamlit_sweethome3d/frontend/public/lib/resources/models/halogenLightSource.png)|
|eTeks#lightSource|Luci|Sorgente luce bianca|![](streamlit_sweethome3d/frontend/public/lib/resources/models/lightSource.png)|
|eTeks#blueLightSource|Luci|Sorgente luce blu|![](streamlit_sweethome3d/frontend/public/lib/resources/models/blueLightSource.png)|
|eTeks#fireglowLightSource|Luci|Sorgente luce fiamma|![](streamlit_sweethome3d/frontend/public/lib/resources/models/fireglowLightSource.png)|
|eTeks#incandescentLightSource|Luci|Sorgente luce incendescente|![](streamlit_sweethome3d/frontend/public/lib/resources/models/incandescentLightSource.png)|
|eTeks#magentaLightSource|Luci|Sorgente luce magenta|![](streamlit_sweethome3d/frontend/public/lib/resources/models/magentaLightSource.png)|
|eTeks#redLightSource|Luci|Sorgente luce rossa|![](streamlit_sweethome3d/frontend/public/lib/resources/models/redLightSource.png)|
|eTeks#greenLightSource|Luci|Sorgente luce verde|![](streamlit_sweethome3d/frontend/public/lib/resources/models/greenLightSource.png)|
|eTeks#curtains|Miscellanea|Bastone per tenda|![](streamlit_sweethome3d/frontend/public/lib/resources/models/curtains.png)|
|eTeks#texturableCylinder|Miscellanea|Cilindro|![](streamlit_sweethome3d/frontend/public/lib/resources/models/cylinder.png)|
|eTeks#texturableBox|Miscellanea|Cubo|![](streamlit_sweethome3d/frontend/public/lib/resources/models/box.png)|
|eTeks#mannequin|Miscellanea|Manichino|![](streamlit_sweethome3d/frontend/public/lib/resources/models/mannequin.png)|
|eTeks#frame|Miscellanea|Quadro|![](streamlit_sweethome3d/frontend/public/lib/resources/models/frame.png)|
|eTeks#railing|Miscellanea|Ringhiera|![](streamlit_sweethome3d/frontend/public/lib/resources/models/railing.png)|
|eTeks#hotWaterRadiator|Miscellanea|Termosifone ad acqua calda|![](streamlit_sweethome3d/frontend/public/lib/resources/models/hotWaterRadiator.png)|
|eTeks#electricRadiator|Miscellanea|Termosifone elettrico|![](streamlit_sweethome3d/frontend/public/lib/resources/models/electricRadiator.png)|
|eTeks#texturableTriangle|Miscellanea|Triangolo|![](streamlit_sweethome3d/frontend/public/lib/resources/models/triangle.png)|
|eTeks#blind|Miscellanea|Veneziana|![](streamlit_sweethome3d/frontend/public/lib/resources/models/blind.png)|
|eTeks#doubleOutwardOpeningWindow|Porte e finestre|Apertura esterna|![](streamlit_sweethome3d/frontend/public/lib/resources/models/doubleOutwardOpeningWindow.png)|
|eTeks#doorFrame|Porte e finestre|Cornice porta|![](streamlit_sweethome3d/frontend/public/lib/resources/models/doorFrame.png)|
|eTeks#roundDoorFrame|Porte e finestre|Cornice porta arrotondata|![](streamlit_sweethome3d/frontend/public/lib/resources/models/roundDoorFrame.png)|
|eTeks#serviceHatch|Porte e finestre|Feritoia|![](streamlit_sweethome3d/frontend/public/lib/resources/models/serviceHatch.png)|
|eTeks#window85x163|Porte e finestre|Finestra|![](streamlit_sweethome3d/frontend/public/lib/resources/models/window85x163.png)|
|eTeks#doubleHungWindow80x122|Porte e finestre|Finestra a ghigliottina|![](streamlit_sweethome3d/frontend/public/lib/resources/models/doubleHungWindow80x122.png)|
|eTeks#halfRoundWindow|Porte e finestre|Finestra a mezzo tondo|![](streamlit_sweethome3d/frontend/public/lib/resources/models/halfRoundWindow.png)|
|eTeks#doubleWindow126x163|Porte e finestre|Finestra doppia|![](streamlit_sweethome3d/frontend/public/lib/resources/models/doubleWindow126x163.png)|
|eTeks#doubleFrenchWindow126x200|Porte e finestre|Finestra doppia francese|![](streamlit_sweethome3d/frontend/public/lib/resources/models/doubleFrenchWindow126x200.png)|
|eTeks#fixedWindow85x123|Porte e finestre|Finestra fissa|![](streamlit_sweethome3d/frontend/public/lib/resources/models/fixedWindow85x123.png)|
|eTeks#frenchWindow85x200|Porte e finestre|Finestra francese|![](streamlit_sweethome3d/frontend/public/lib/resources/models/frenchWindow85x200.png)|
|eTeks#window85x123|Porte e finestre|Finestra Piccola|![](streamlit_sweethome3d/frontend/public/lib/resources/models/window85x123.png)|
|eTeks#doubleWindow126x123|Porte e finestre|Finestra piccola doppia|![](streamlit_sweethome3d/frontend/public/lib/resources/models/doubleWindow126x123.png)|
|eTeks#sliderWindow126x200|Porte e finestre|Finestra scorrevole|![](streamlit_sweethome3d/frontend/public/lib/resources/models/sliderWindow126x200.png)|
|eTeks#roundWindow|Porte e finestre|Finestra tonda|![](streamlit_sweethome3d/frontend/public/lib/resources/models/roundWindow.png)|
|eTeks#fixedTriangleWindow85x85|Porte e finestre|Finestra triangolare|![](streamlit_sweethome3d/frontend/public/lib/resources/models/fixedTriangleWindow85x85.png)|
|eTeks#door|Porte e finestre|Porta|![](streamlit_sweethome3d/frontend/public/lib/resources/models/door.png)|
|eTeks#openDoor|Porte e finestre|Porta aperta|![](streamlit_sweethome3d/frontend/public/lib/resources/models/openDoor.png)|
|eTeks#roundedDoor|Porte e finestre|Porta arrotondata|![](streamlit_sweethome3d/frontend/public/lib/resources/models/roundedDoor.png)|
|eTeks#frontDoor|Porte e finestre|Porta di ingresso|![](streamlit_sweethome3d/frontend/public/lib/resources/models/frontDoor.png)|
|eTeks#garageDoor|Porte e finestre|Porta garage|![](streamlit_sweethome3d/frontend/public/lib/resources/models/garageDoor.png)|
|eTeks#staircase|Scale|Scala|![](streamlit_sweethome3d/frontend/public/lib/resources/models/staircase.png)|
|eTeks#spiralStaircase|Scale|Scala a chiocciola|![](streamlit_sweethome3d/frontend/public/lib/resources/models/spiralStaircase.png)|
|eTeks#curveStaircase|Scale|Scala curva|![](streamlit_sweethome3d/frontend/public/lib/resources/models/curveStaircase.png)|
|eTeks#aquarium|Soggiorno|Acquario|![](streamlit_sweethome3d/frontend/public/lib/resources/models/aquarium.png)|
|eTeks#fireplace|Soggiorno|Camino|![](streamlit_sweethome3d/frontend/public/lib/resources/models/fireplace.png)|
|eTeks#dresser|Soggiorno|Credenza|![](streamlit_sweethome3d/frontend/public/lib/resources/models/dresser.png)|
|eTeks#sofa|Soggiorno|Divano|![](streamlit_sweethome3d/frontend/public/lib/resources/models/sofa.png)|
|eTeks#sofa2|Soggiorno|Divano|![](streamlit_sweethome3d/frontend/public/lib/resources/models/sofa2.png)|
|eTeks#cornerSofa|Soggiorno|Divano angolare|![](streamlit_sweethome3d/frontend/public/lib/resources/models/cornerSofa.png)|
|eTeks#flowers|Soggiorno|Fiori|![](streamlit_sweethome3d/frontend/public/lib/resources/models/flowers.png)|
|eTeks#laptop|Soggiorno|Laptop|![](streamlit_sweethome3d/frontend/public/lib/resources/models/laptop.png)|
|eTeks#bookcase|Soggiorno|Libreria|![](streamlit_sweethome3d/frontend/public/lib/resources/models/bookcase.png)|
|eTeks#filledBookcase|Soggiorno|Libreria|![](streamlit_sweethome3d/frontend/public/lib/resources/models/filledBookcase.png)|
|eTeks#piano|Soggiorno|Pianoforte|![](streamlit_sweethome3d/frontend/public/lib/resources/models/piano.png)|
|eTeks#plant|Soggiorno|Pianta|![](streamlit_sweethome3d/frontend/public/lib/resources/models/plant.png)|
|eTeks#armchair|Soggiorno|Poltrona|![](streamlit_sweethome3d/frontend/public/lib/resources/models/armchair.png)|
|eTeks#armchair2|Soggiorno|Poltrona|![](streamlit_sweethome3d/frontend/public/lib/resources/models/armchair2.png)|
|eTeks#desk|Soggiorno|Scrivania|![](streamlit_sweethome3d/frontend/public/lib/resources/models/desk.png)|
|eTeks#chair|Soggiorno|Sedia|![](streamlit_sweethome3d/frontend/public/lib/resources/models/chair.png)|
|eTeks#chair2|Soggiorno|Sedia|![](streamlit_sweethome3d/frontend/public/lib/resources/models/chair2.png)|
|eTeks#stool|Soggiorno|Sgabello|![](streamlit_sweethome3d/frontend/public/lib/resources/models/stool.png)|
|eTeks#coffeeTable|Soggiorno|Tavolino|![](streamlit_sweethome3d/frontend/public/lib/resources/models/coffeeTable.png)|
|eTeks#table|Soggiorno|Tavolo|![](streamlit_sweethome3d/frontend/public/lib/resources/models/table.png)|
|eTeks#squareTable|Soggiorno|Tavolo quadrato|![](streamlit_sweethome3d/frontend/public/lib/resources/models/squareTable.png)|
|eTeks#roundTable|Soggiorno|Tavolo rotondo|![](streamlit_sweethome3d/frontend/public/lib/resources/models/roundTable.png)|
|eTeks#tvUnit|Soggiorno|Televisione|![](streamlit_sweethome3d/frontend/public/lib/resources/models/tvUnit.png)|
|eTeks#flatTV|Soggiorno|TV LCD|![](streamlit_sweethome3d/frontend/public/lib/resources/models/flatTV.png)|
|eTeks#glassDoorCabinet|Soggiorno|Vetrina|![](streamlit_sweethome3d/frontend/public/lib/resources/models/glassDoorCabinet.png)|
|eTeks#chest|Stanza da letto|Cassettiera|![](streamlit_sweethome3d/frontend/public/lib/resources/models/chest.png)|
|eTeks#bedsideTable|Stanza da letto|Comodino|![](streamlit_sweethome3d/frontend/public/lib/resources/models/bedsideTable.png)|
|eTeks#wardrobe|Stanza da letto|Guardaroba|![](streamlit_sweethome3d/frontend/public/lib/resources/models/wardrobe.png)|
|eTeks#crib|Stanza da letto|Lettino|![](streamlit_sweethome3d/frontend/public/lib/resources/models/crib.png)|
|eTeks#bed|Stanza da letto|Letto|![](streamlit_sweethome3d/frontend/public/lib/resources/models/bed.png)|
|eTeks#bed140x190|Stanza da letto|Letto 140x190|![](streamlit_sweethome3d/frontend/public/lib/resources/models/bed140x190.png)|
|eTeks#bed90x190|Stanza da letto|Letto 90x190|![](streamlit_sweethome3d/frontend/public/lib/resources/models/bed90x190.png)|
|eTeks#bunkBed90x190|Stanza da letto|Letto a castello|![](streamlit_sweethome3d/frontend/public/lib/resources/models/bunkBed90x190.png)|
|eTeks#cornerBunkBed90x190|Stanza da letto|Letto a castello angolare|![](streamlit_sweethome3d/frontend/public/lib/resources/models/cornerBunkBed90x190.png)|
|eTeks#loftBed140x190|Stanza da letto|Letto alto 140x190|![](streamlit_sweethome3d/frontend/public/lib/resources/models/loftBed140x190.png)|
|eTeks#slidingDoors|Stanza da letto|Porte scorrevoli|![](streamlit_sweethome3d/frontend/public/lib/resources/models/slidingDoors.png)|
