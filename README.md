[![pipeline status](https://gitlab.com/nicolalandro/streamlit-sweethome3d/badges/main/pipeline.svg)](https://gitlab.com/nicolalandro/streamlit-sweethome3d/-/commits/main) 

[![License: GPLv3](https://img.shields.io/badge/license-GPLv3-lightgray)](LICENSE)
[![pypi package](https://img.shields.io/badge/pypi-streamlit_sweethome3d-blue)](https://pypi.org/project/streamlit-sweethome3d/)
[![pypi downloads numbers](https://img.shields.io/pypi/dm/streamlit-sweethome3d.svg)](https://pypistats.org/packages/streamlit-sweethome3d)



[![Open in Huggingface](https://huggingface.co/datasets/huggingface/badges/raw/refs%2Fpr%2F11/open-in-hf-spaces-md-dark.svg)](https://huggingface.co/spaces/z-uo/SweetHome3DPlanner)


# Streamlit SweetHome3D
This project have the aim to port an house creator and furniture planner to streamlit in order to use it as a component of some demos including a good view with basic features quickly.

![Demo screenshot](examples/imgs/screen.png)

* install with gitlab registry
```
pip install streamlit-sweethome3d
# from gitlab
pip install streamlit-sweethome3d --index-url https://gitlab.com/api/v4/projects/53115935/packages/pypi/simple
```
* code example here (full [furniture doc](FURNITURE_DOC.md))
```
import streamlit as st
from streamlit_sweethome3d import streamlit_sweethome3d

import json

st.header("Sweet Home 3D")
st.markdown("This is an example of [streamlit_sweethome3d](https://gitlab.com/nicolalandro/streamlit-sweethome3d) plugin.")

in_state = {
    "homes": [
      {
        "walls": [
          [0, 180, 0, 400, 20, 250], # params: x1, y1 x2, y2, spessore, altezza
          [350, 180, 350, 400, 20, 250],
          [0-10, 180, 350+10, 180, 20, 250]
        ],
        "rooms": [
          [[0,180], [350, 180], [350, 400], [0, 400]] # points
        ],
        "furnitures": [
          {
            "id": "eTeks#shower", # complete doc here https://gitlab.com/nicolalandro/streamlit-sweethome3d/-/blob/main/FURNITURE_DOC.md
            "x": 50,
            "y": 230,
            "elevation": 0,
          }
        ]
      }
    ]
}

out_state = streamlit_sweethome3d(state = in_state, out_state=True)

col1, col2 = st.columns(2)
with col1:
    st.text('in state')
    st.code(json.dumps(in_state, indent=2))
with col2:
    st.text('out state')
    st.code(out_state)
```

## TODO List
Here I can explain the simple plan that I want to follow for this plugin:

* [x] create basic streamlit plugin with docker
* [x] basic plugin view only
* [x] build pipeline gitlab
* [x] deploy oh Hugging Face Spaces
* [x] Bug Fix: the deploy version do not load correctly (from 0.0.2 fixed)
* [ ] basic in settings during creation
  * [x] walls
  * [x] rooms
  * [x] furnitures (from 0.0.3)
  * [x] furnitures list in doc
  * [ ] setup camera
  * [ ] switch and setup observer camera
* [ ] basic out settings
  * [x] walls
  * [x] rooms
  * [x] furnitures
  * [ ] camera
* [x] pypi release
* [ ] expands supported features (like create new layer and more)
* [ ] documentation
  * [x] basic code examples
  * [ ] complete guide uses with docker and custom models instead standard
* [ ] automation testing
* [ ] Bug Fix: conflict between zoom and scroll in 3D view

## Develop
This is the guide for developers. 
To run for development mode:
```
docker-compose build
docker-compose up
# streamlit at localhost:8000
# frontend at localhost:5173
# demo with lib installed at localhost:8001
```

### Frontend JS
* open terminal in container
```
docker-compose run frontend ash
$ chmod -R 777 *
$ ...
```

### Streamlit
* open terminal in streamlit container
```
docker-compose run streamlit bash
```

### Deploy PiPy

```
python3 -m venv venv
source venv/bin/activate.fish
pip install -e .

pip install keyring
keyring set https://upload.pypi.org/legacy/ <pypi uname>
```
* prepare your ~/.pypirc file
```
[pypi]
repository=https://upload.pypi.org/legacy/
username = __token__
password = <token>
```
* build frontend
```
docker-compose run frontend ash
$ rm -rf dist
$ npm run build
$ chmod -R 777 *
```
* build backend
```
source venv/bin/activate.fish
pip install twine

rm -rf dist
python setup.py sdist
twine check dist/*
```
* upload
```
twine upload dist/*
```

## References
* [python](https://www.python.org/): programming language for develop the tool
* [HTML](https://www.w3schools.com/html/): markup language for the output file generated by the tool
* [JavaScript](https://www.w3schools.com/js/): programming language used into the HTML for logic implementation 
* [vite](https://vitejs.dev/): js builder
* [streamlit](https://streamlit.io/): python library to make quick and dirty UI
* [vite vanilla js streamlit component](https://dev.to/aisone/streamlit-custom-components-vite-vanilla-js-40hl): how to create vanilla js plugin for streamlit
* [How to write setup.py and deploy on pypi](https://medium.com/@joel.barmettler/how-to-upload-your-python-package-to-pypi-65edc5fe9c56): important for deploy the code as python lib
* [Gitlab CI/CD for python registry](https://docs.gitlab.com/ee/user/packages/pypi_repository/): how to setup pipeline to gitlab python registry
* [Sweet Home 3D js sudy repo](https://gitlab.com/nicolalandro/study-sweethome3djs): a study about sweet home 3D
    * [SweetHome3DJS](https://sourceforge.net/projects/sweethome3d/files/SweetHome3DJS-source/): Web version of sweet home 3D
    * [forum](http://www.sweethome3d.com/support/forum/viewthread_thread,10403_offset,0): info forum
    * [Builded](https://sourceforge.net/projects/sweethome3d/files/SweetHome3DJS/)
    * [forum](http://www.sweethome3d.com/support/forum/viewthread_thread,12791_lastpage,yes#60399): info asked about build and other things
    * [forum](http://www.sweethome3d.com/support/forum/viewthread_thread,12834_lastpage,yes#60705): annunced that lib
   